package com.example.ezana.placebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class HistoryActivity extends ActionBarActivity implements ActionMode. Callback {
    private ListView mListview ;
    protected Object mActionMode ;
    public int selectedItem = -1;
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        mListview = ( ListView ) findViewById (R. id . list );
        mListview . setOnItemLongClickListener ( new AdapterView . OnItemLongClickListener () {
            @Override
            public boolean onItemLongClick ( AdapterView <? > parent , View view , int position , long
                    id ) {
                if ( mActionMode != null )
                    return false ;
                selectedItem = position ;
                mActionMode = HistoryActivity . this . startActionMode ( HistoryActivity . this );
                view . setSelected ( true );
                return true ;
            }
        }) ;
    }

    @Override
    public boolean onCreateActionMode ( ActionMode mode , Menu menu ) {
        MenuInflater inflater = mode . getMenuInflater () ;
        inflater . inflate (R . menu . rowselection , menu );
        return true ;
    }
    @Override
    public boolean onPrepareActionMode ( ActionMode mode , Menu menu ) {
        return false ;
    }
    @Override
    public boolean onActionItemClicked ( ActionMode mode , MenuItem item ) {
        switch ( item . getItemId () ) {
            case R. id . action_delete_place :
// Delete Item
                mode . finish () ;
                return true ;
            default : return false ;
        }
    }
    @Override
    public void onDestroyActionMode ( ActionMode mode ) {
        mActionMode = null ;
        selectedItem = -1;
    }
}