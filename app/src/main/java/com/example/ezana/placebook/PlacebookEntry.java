package com.example.ezana.placebook;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ezana on 5/25/2015.
 */
public class PlacebookEntry{
    public String name;
    public String description;
    public Bitmap photo;

    @Override
    public String toString() {
        return name + "\n" + description;
    }

    public Bitmap getImageUrl() {
        return photo;
    }

    public void setImageUrl(Bitmap imageUrl) {
        photo = imageUrl;
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String title) {
        name = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        description = description;
    }



}

