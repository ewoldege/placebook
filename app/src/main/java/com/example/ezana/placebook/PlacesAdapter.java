package com.example.ezana.placebook;

import android.content.Context;
import android.support.v7.internal.view.menu.MenuView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mohamed on 6/6/2015.
 */
public class PlacesAdapter extends ArrayAdapter<PlacebookEntry> {
    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView home;
        ImageView image;
    }

    public PlacesAdapter(Context context, ArrayList<PlacebookEntry> users) {
        super(context, R.layout.row_layout, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        PlacebookEntry placebookentry = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_layout, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.row_txtPlace);
            viewHolder.home = (TextView) convertView.findViewById(R.id.row_txtPlaceDesc);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.row_image_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.name.setText(placebookentry.name);
        viewHolder.home.setText(placebookentry.description);
        viewHolder.image.setImageBitmap(placebookentry.photo);
        // Return the completed view to render on screen
        return convertView;
    }


}
