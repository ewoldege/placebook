package com.example.ezana.placebook;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;

import java.util.ArrayList;


public class SavedPlacesActivity extends ActionBarActivity implements ActionMode.Callback {
    ListView listView;
    PlacesAdapter adapter;
    protected Object mActionMode;
    boolean deleteFlag = false;
    public int selectedItem = -1;
    public ArrayList<PlacebookEntry> placeEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //Intent i = getIntent();
        //Bundle args = i.getBundleExtra("BUNDLE");
        placeEntries = MainActivity.placeEntries;

        listView = (ListView) findViewById(R.id.list);
        String[] values = new String[]{"Android List View",
                "Adapter implementation",
                "Simple List View In Android",
                "Create List View Android",
                "Android Example",
                "List View Source Code",
                "List View Array Adapter",
                "Android Example List View"
        };

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, values);
        adapter = new PlacesAdapter(this, placeEntries);
        // Assign adapter to ListView
        listView.setAdapter(adapter);
        listView.setLongClickable(true);

        listView
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                    public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                   int pos, long id) {
                        // TODO Auto-generated method stub
                        if (mActionMode != null)
                            return false;
                        selectedItem = pos;
                        mActionMode = SavedPlacesActivity.this.startActionMode(SavedPlacesActivity.this);
                        arg1.setSelected(true);

                        return true;
                    }
                });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_new_place:
                this.finish();
                return true;
            case R.id.action_delete_place:
                deleteFlag = true;
                return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.rowselection, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete_place:
                placeEntries.remove(selectedItem);
                adapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Entry Deleted",Toast.LENGTH_SHORT).show();
// Delete Item
                mode.finish();
                return true;
            case R.id.action_edit_place:
                MainActivity.editMode = true;
                MainActivity.listItem = selectedItem;
                mode.finish();
                this.finish();
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = null;
        selectedItem = -1;
    }
}

