package com.example.ezana.placebook;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.speech.RecognizerIntent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlacePicker;

public class MainActivity extends ActionBarActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static boolean editMode = false;
    public static int listItem = -1;
    private GoogleApiClient mGoogleApiClient;
    private static final int CAMERA_PIC_REQUEST = 22;
    private static final int PLACE_PICKER_REQUEST = 1;
    public static ArrayList<PlacebookEntry> placeEntries = new ArrayList<>();
    Uri cameraUri;
    Bitmap bitmapPictures;
    Place places;
    ImageButton cameraButton,locationButton,saveButton,voiceButton;
    private ImageView ImgPhoto2;
    private String mCurrentPhotoPath,stringLatLng,txtPlace,txtDesc ;
    File photoFile=null;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    private boolean placePickerUsed;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";
    private final int REQ_CODE_SPEECH_INPUT = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        initGoogleApi();
        mainScreenSetup();
        if(editMode){
            showEntry(listItem);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    private void mainScreenSetup(){
        setContentView(R.layout.activity_main);
        cameraButton = (ImageButton) findViewById(R.id.button_snapshot);
        locationButton = (ImageButton) findViewById(R.id.button_location);
        voiceButton = (ImageButton) findViewById(R.id.button_speak);
        saveButton = (ImageButton) findViewById(R.id.saved);
        cameraButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                dispatchCameraActivity();
            }
        });
        locationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPlacePicker();

            }
        });

        saveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                saveEntry();
            }
        });

        voiceButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                voiceStart();
            }
        });
    }

    public void voiceStart(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void dispatchCameraActivity(){
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File

        }
        if(photoFile!=null) {
            try {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraUri.fromFile(photoFile);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Couldn't load photo", Toast.LENGTH_LONG).show();
            }
        }
    }
    private void saveEntry(){
        boolean sendFlag = false;
        PlacebookEntry entry = new PlacebookEntry();

        if(editMode){
            if(placePickerUsed)
                placeEntries.get(listItem).name = places.getName().toString();
            else
                placeEntries.get(listItem).name = ((EditText) findViewById(R.id.txtPlaceContent)).getText().toString();

            placeEntries.get(listItem).description = ((EditText) findViewById(R.id.edit_place_desc)).getText().toString();
            if(bitmapPictures!=null)
                placeEntries.get(listItem).photo = bitmapPictures;
            else {
                Toast.makeText(getApplicationContext(), "Edited Entry Saved without Picture!", Toast.LENGTH_SHORT).show();
                sendFlag = true;
            }
            Toast.makeText(getApplicationContext(), "Entry Edited!", Toast.LENGTH_SHORT).show();
            reset();
            Intent i = new Intent(this, SavedPlacesActivity.class);
            startActivity(i);
        }
else {
            if (placePickerUsed)
                entry.name = places.getName().toString();
            else
                entry.name = ((EditText) findViewById(R.id.txtPlaceContent)).getText().toString();

            entry.description = ((EditText) findViewById(R.id.edit_place_desc)).getText().toString();
            if (bitmapPictures != null)
                entry.photo = bitmapPictures;
            else {
                Toast.makeText(getApplicationContext(), "Entry Saved without Picture!", Toast.LENGTH_SHORT).show();
                sendFlag = true;
            }
            placeEntries.add(entry);
            reset();
            if (bitmapPictures != null)
                Toast.makeText(getApplicationContext(), "PlaceBook Entry Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private void reset(){
        ((EditText) findViewById(R.id.txtPlaceContent)).setText("");
        ((EditText) findViewById(R.id.edit_place_desc)).setText("");
        ((TextView) findViewById(R.id.txtGpsLatitude)).setText("Latitude");
        ((TextView) findViewById(R.id.txtGpsLongitude)).setText("Longitude");
        placePickerUsed = false;
        places = null;
        bitmapPictures = null;
        editMode = false;
    }
    private void showEntry(int item){
        ((TextView) findViewById(R.id.txtPlaceContent)).setText(placeEntries.get(item).name);
        ((TextView) findViewById(R.id.edit_place_desc)).setText(placeEntries.get(item).description);
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case CAMERA_PIC_REQUEST:
                    if (resultCode == RESULT_OK) {
                        try {
                            bitmapPictures = (Bitmap) data.getExtras().get("data");

                        } catch (Exception e) {
                            Toast.makeText(this, "Couldn't load photo", Toast.LENGTH_LONG).show();
                        }
                    }
                    break;
                case PLACE_PICKER_REQUEST:
                    placePickerUsed = true;
                    Place place = PlacePicker . getPlace ( data , this ) ;
                    places = place;
                    stringLatLng = (place.getLatLng()).toString();
                    ((TextView)findViewById(R.id.txtGpsLatitude)).setText("Latitude: " + stringLatLng.substring(10, 18));
                    ((TextView)findViewById(R.id.txtGpsLongitude)).setText("Longitude: " + stringLatLng.substring(21, 32));
                    // Set place name text view to place . getName () .
                    break;
                case REQ_CODE_SPEECH_INPUT: {
                    if (resultCode == RESULT_OK && null != data) {

                        ArrayList<String> result = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        ((TextView)findViewById(R.id.txtPlaceContent)).setText(result.get(0));
                    }
                    break;
                }

                default:
                    break;
            }
        } catch (Exception e) {
            //Hello
        }
        // For Google Maps
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.action_new_place:
                mainScreenSetup();
                placePickerUsed = false;
                return true;
            case R.id.action_view_all:
                Intent i = new Intent(this, SavedPlacesActivity.class);
                //Bundle args = new Bundle();
                //args.putSerializable("ARRAYLIST",placeEntries);
                //i.putExtra("BUNDLE",args);
                startActivity(i);
                //viewAll();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initGoogleApi () {
        mGoogleApiClient = new GoogleApiClient
                . Builder ( this )
                . addApi(Places.GEO_DATA_API)
                . addApi(Places.PLACE_DETECTION_API)
                . addConnectionCallbacks(this)
                . addOnConnectionFailedListener(this)
                . build() ;
        System.out.println("googleInit");
    }
    // Call l a u n c h P l a c e P i c k e r () when the Pick -A - Place button is clicked .
    private void launchPlacePicker () {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        Context context = getApplicationContext();

        try {
            startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
        } catch ( GooglePlayServicesRepairableException e) {
            Toast.makeText(this,"Repairable Exception",Toast.LENGTH_SHORT);
// Handle exception - Display a Toast message
        } catch ( GooglePlayServicesNotAvailableException e ) {
            Toast.makeText(this,"Google Services not Available",Toast.LENGTH_SHORT);
// Handle exception - Display a Toast message
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }

    // The rest of this code is all about building the error dialog

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(),"ErrorDialog");
        // getSupportFragmentManager doesn't work
    }



    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity)getActivity()).onDialogDismissed();
        }
    }

}